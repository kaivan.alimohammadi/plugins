<?php

class Admin {
	//private $dashboard_page_hook;
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'init_admin_menu' ) );
		//add_action('admin_enqueue_scripts',array($this,'add_assets'));
		//add_action("load-{$this->dashboard_page_hook}");
		add_action('wp_ajax_nopriv_save_settings',[$this,'save_settings_by_ajax']);
		add_action('wp_ajax_save_settings',[$this,'save_settings_by_ajax']);
	}

	public function init_admin_menu() {
		$dashboard_page      = new Admin_Dashboard();
		$lists_page = new Admin_Lists();
		$dashboard_page_hook = add_menu_page( 'مدیریت اشتراک ها',
			'اشتراک ها',
			'manage_options',
			'subscribers',
			array( $dashboard_page, 'index' )
		);
		add_submenu_page( 'subscribers',
			'مدیریت اشتراک ها',
			'داشبورد',
			'manage_options',
			'subscribers'
		);
		$lists_page_hook=add_submenu_page( 'subscribers',
			'مدیریت اشتراک ها',
			'لیست درخواست ها',
			'manage_options',
			'subscribers_list',
		array($lists_page,'index')
		);


		add_action( "load-{$dashboard_page_hook}", function () {
			$this->add_assets();
		} );
		add_action("load-{$lists_page_hook}",function (){
			$this->add_assets();
		});
	}

	public function add_assets() {
		wp_register_style( 'subscribe_main_style', SUBSCRIBE_CSS . 'main.css' );
		wp_register_script(
			'subscribe_main_script',
			SUBSCRIBE_JS . 'subscribe_main.js',
			array( 'jquery' )
		);
		wp_register_style( 'uikit_rtl_style', SUBSCRIBE_COMPONENTS . 'uikit/uikit-rtl.min.css' );
		wp_register_script(
			'uikit_js',
			SUBSCRIBE_COMPONENTS . 'uikit/uikit.min.js',
			array( 'jquery' )
		);


		wp_enqueue_style( 'subscribe_main_style' );
		wp_enqueue_script( 'subscribe_main_script' );
		wp_enqueue_style( 'uikit_rtl_style' );
		wp_enqueue_script( 'uikit_js' );
		wp_localize_script('subscribe_main_script','subscribe_data',[
			'ajax_url' => admin_url('admin-ajax.php'),
			'per_page' => 20,
			'nonce' => wp_create_nonce('save_settings_ajax')
		]);
		wp_enqueue_media();
	}

	public function save_settings_by_ajax() {
		check_ajax_referer('save_settings_ajax','nonce');
		$settings = intval($_POST['setting_value']);
		if(!$settings){
			wp_send_json([
				'success' => false,
				'message' => 'اطلاعات ناقص می باشد.'
			]);
		}
		update_option('settings_value',$settings);
		wp_send_json([
			'success' => true,
			'message' => 'اطلاعات با موفقیت ذخیره شد.'
		]);
	}

}

new Admin();